Compatibility
*************

ssh-deploy-key is a Python application.  Python is installed by default on most
systems.  However, on old OSes, you may need to update your Python version.

Python Versions Supported
=========================

ssh-deploy-key is currently only compatible with
cpython.  Versions supported:

 * python 2.7
 * python 3.3 or higher


Unsupportable Python Versions
=============================

SSH Deploy Key uses the Paramiko ssh library.  Paramiko
is not a "pure python" solution.  It has
binary dependencies that make it incompatible with
non-cpython implementations, including:

 * jython
 * pypy

